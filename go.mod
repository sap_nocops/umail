module gitlab.com/sap_nocops/umail

go 1.16

require (
	github.com/emersion/go-imap v1.0.6
	github.com/emersion/go-message v0.11.1
	github.com/emersion/go-sasl v0.0.0-20200509203442-7bfe0ed36a21
	github.com/emersion/go-smtp v0.14.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/nanu-c/qml-go v0.0.0-20201002212753-238e81315528
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	golang.org/x/sys v0.0.0-20210303074136-134d130e1a04 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
)
