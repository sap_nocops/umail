package main

import (
	"database/sql"
	"github.com/sirupsen/logrus"
	"gitlab.com/sap_nocops/umail/pkg/middleware"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"gitlab.com/sap_nocops/umail/pkg/provider"
	"time"
)

func main() {
	//TODO get path from library
	//TODO init db in case it does not exist
	db, err := sql.Open("sqlite3", "test_data/test.db")
	if err != nil {
		panic(err)
	}
	accountProvider := provider.NewAccountProviderSqlite(db)
	accounts, err := accountProvider.GetAll()
	if err != nil {
		panic(err)
	}
	syncs := make(map[string]middleware.ImapSqliteSynchronizer)
	for _, account := range accounts {
		mailProvider, err := provider.NewMailProviderImap(account, true)
		if err != nil {
			panic(err)
		}
		localMessageProvider := provider.NewMessageProviderSqlite(account, db)
		mailboxProvider := provider.NewMailboxProviderSqlite(db, account)
		syncs[account.Email] = middleware.ImapSqliteSynchronizer{
			Account:             account,
			MailProvider:        mailProvider,
			RemoteMessageReader: mailProvider,
			LocalMessageReader:  localMessageProvider,
			MessageWriter:       localMessageProvider,
			MailboxReader:       mailProvider,
			MailboxWriter:       mailboxProvider,
		}
	}
	for email, sync := range syncs {
		logrus.Infof("synching mailboxes for %s\n", email)
		err := sync.SyncMailBoxes()
		if err != nil {
			logrus.Warnf("error synching mailbox for %s", email)
		}
	}
	//TODO get timing from configuration
	timer := time.NewTimer(2 * time.Second)
	for {
		<-timer.C
		for email, sync := range syncs {
			logrus.Debugf("synching %s\n", email)
			//TODO we should know which mailbox to sync periodically for each account
			err := sync.SyncMail(model.Mailbox{Name: "INBOX"})
			if err != nil {
				logrus.Warnf("error synching mail for %s INBOX", email)
			}
		}
	}
}
