package model

import (
	"fmt"
	"strings"
	"time"
)

type Profile struct {
	Name string
}

type Account struct {
	Host     string
	Port     int
	Email    string
	Password string
	Username string
}

type Mailbox struct {
	Name string
}

type Message struct {
	Uid       uint32
	Id        string
	Subject   string
	From      []string
	To        []string
	Cc        []string
	Bcc       []string
	ReplyTo   []string
	Sender    []string
	Date      time.Time
	InReplyTo string
}

func (m *Message) ToString() string {
	return fmt.Sprintf(`
		uid: %d
		id: %s
		date: %s
		sender: %s
		from: %s
		to: %s
		reply_to: %s
		in_reply_to: %s
		subject: %s
		cc: %s
		bcc: %s`,
		m.Uid, m.Id, m.Date.String(), strings.Join(m.Sender, ", "),
		strings.Join(m.From, ", "), strings.Join(m.To, ", "),
		m.InReplyTo, strings.Join(m.ReplyTo, ", "),
		m.Subject, m.Cc, m.Bcc)
}

type MessageBody struct {
	Text string
}

type Attachment struct {
	Filename string
	Body     []byte
}
