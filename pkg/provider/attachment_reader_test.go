package provider

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"io/ioutil"
	"testing"
)

func TestAttachmentProviderFs_GetMessageAttachment(t *testing.T) {
	type fields struct {
		account  model.Account
		basePath string
	}
	type args struct {
		mailbox        model.Mailbox
		uid            uint32
		attachmentName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Attachment
		wantErr bool
	}{
		{
			name: "shouldReadAttachmentContent",
			fields: fields{
				account: model.Account{
					Email: "sap@mail.com",
				},
				basePath: "test_data",
			},
			args: args{
				mailbox:        model.Mailbox{Name: "INBOX"},
				uid:            1,
				attachmentName: "attach.txt",
			},
			want: model.Attachment{
				Filename: "attach.txt",
				Body:     mustReadFile(t, "test_data/sap@mail.com/INBOX/1/attach.txt"),
			},
			wantErr: false,
		},
		{
			name: "shouldErrorWhenFileNotFound",
			fields: fields{
				account: model.Account{
					Email: "sap@mail.com",
				},
				basePath: "test_data",
			},
			args: args{
				mailbox:        model.Mailbox{Name: "INBOX"},
				uid:            1,
				attachmentName: "non_existing_attach.txt",
			},
			want:    model.Attachment{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AttachmentProviderFs{
				account:  tt.fields.account,
				basePath: tt.fields.basePath,
			}
			got, err := a.GetMessageAttachment(tt.args.mailbox, tt.args.uid, tt.args.attachmentName)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessageAttachment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestAttachmentProviderFs_GetMessageAttachments(t *testing.T) {
	type fields struct {
		account  model.Account
		basePath string
	}
	type args struct {
		mailbox model.Mailbox
		uid     uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "shouldListAttachments",
			fields: fields{
				account: model.Account{
					Email: "sap@mail.com",
				},
				basePath: "test_data",
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				uid: 1,
			},
			want:    []string{"attach.pdf", "attach.txt"},
			wantErr: false,
		},
		{
			name: "shouldErrorWhenDirectoryNotFound",
			fields: fields{
				account: model.Account{
					Email: "sap@mail.com",
				},
				basePath: "test_data",
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				uid: 3,
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AttachmentProviderFs{
				account:  tt.fields.account,
				basePath: tt.fields.basePath,
			}
			got, err := a.GetMessageAttachments(tt.args.mailbox, tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessageAttachments() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, got, tt.want)
		})
	}
}

func mustReadFile(t *testing.T, path string) []byte {
	file, err := ioutil.ReadFile(path)
	require.NoError(t, err)
	return file
}
