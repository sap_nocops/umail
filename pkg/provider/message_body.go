package provider

import (
	"database/sql"
	"gitlab.com/sap_nocops/umail/pkg/model"
)

type MessageBodyReaderProvider interface {
	GetMessageBody(mailbox model.Mailbox, uid uint32) (model.MessageBody, error)
}

type MessageBodyWriterProvider interface {
	SaveMessageBody(mailbox model.Mailbox, uid uint32, body model.MessageBody) error
}

type MessageBodyProviderSqlite struct {
	account model.Account
	db      *sql.DB
}

func (m *MessageBodyProviderSqlite) GetMessageBody(mailbox model.Mailbox, uid uint32) (model.MessageBody, error) {
	row := m.db.QueryRow("SELECT body FROM message_bodies WHERE email = $1 and uid = $2 and mailbox = $3", m.account.Email, uid, mailbox.Name)
	if row.Err() != nil {
		return model.MessageBody{}, row.Err()
	}
	var body string
	err := row.Scan(&body)
	if err != nil {
		return model.MessageBody{}, err
	}
	return model.MessageBody{
		Text: body,
	}, nil
}

func (m *MessageBodyProviderSqlite) SaveMessageBody(mailbox model.Mailbox, uid uint32, body model.MessageBody) error {
	_, err := m.db.Exec("INSERT INTO message_bodies (email, uid, mailbox, body) VALUES ($1, $2, $3, $4)", m.account.Email, uid, mailbox.Name, body.Text)
	return err
}
