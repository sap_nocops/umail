package provider

import (
	"errors"
	"github.com/emersion/go-smtp"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"io"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

// The Backend implements SMTP server methods.
type Backend struct{}

// Login handles a login command with username and password.
func (bkd *Backend) Login(state *smtp.ConnectionState, username, password string) (smtp.Session, error) {
	if username != "username" || password != "password" {
		return nil, errors.New("Invalid username or password")
	}
	return &Session{}, nil
}

// AnonymousLogin requires clients to authenticate using SMTP AUTH before sending emails
func (bkd *Backend) AnonymousLogin(state *smtp.ConnectionState) (smtp.Session, error) {
	return nil, smtp.ErrAuthRequired
}

// A Session is returned after successful login.
type Session struct{}

func (s *Session) Mail(from string, opts smtp.MailOptions) error {
	log.Println("Mail from:", from)
	return nil
}

func (s *Session) Rcpt(to string) error {
	log.Println("Rcpt to:", to)
	return nil
}

func (s *Session) Data(r io.Reader) error {
	if b, err := ioutil.ReadAll(r); err != nil {
		return err
	} else {
		log.Println("Data:", string(b))
	}
	return nil
}

func (s *Session) Reset() {}

func (s *Session) Logout() error {
	return nil
}

func startUpSmtpServer() *smtp.Server {
	be := &Backend{}

	s := smtp.NewServer(be)

	s.Addr = ":1025"
	s.Domain = "localhost"
	s.ReadTimeout = 10 * time.Second
	s.WriteTimeout = 10 * time.Second
	s.MaxMessageBytes = 1024 * 1024
	s.MaxRecipients = 50
	s.AllowInsecureAuth = true

	log.Println("Starting server at", s.Addr)
	go func() {
		if err := s.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()
	return s
}

func TestSmtpMessageSender_Send(t *testing.T) {
	server := startUpSmtpServer()
	defer server.Close()
	type args struct {
		message model.Message
		body    model.MessageBody
		mailbox model.Mailbox
	}
	type fields struct {
		account model.Account
	}
	tests := []struct {
		name    string
		args    args
		fields  fields
		wantErr bool
	}{
		{
			name: "should send message",
			fields: fields{
				account: model.Account{
					Host:     "localhost",
					Port:     1025,
					Email:    "sender@example.org",
					Password: "password",
					Username: "username",
				},
			},
			args: args{
				message: model.Message{
					Subject: "test",
					From:    []string{"sender@example.org"},
					To:      []string{"receiver@example.org"},
					Sender:  []string{"sender@example.org"},
				},
				body: model.MessageBody{
					Text: "Hello!",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &MailProviderImapSmtp{
				smtpAccount: tt.fields.account,
			}
			if err := s.CreateMessage(tt.args.mailbox, tt.args.message, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("Send() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
