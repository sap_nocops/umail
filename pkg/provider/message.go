package provider

import (
	"context"
	"database/sql"
	"fmt"
	"gitlab.com/sap_nocops/umail/pkg/model"
)

type MessageReaderProvider interface {
	GetMessagesUids(mailbox model.Mailbox) ([]uint32, error)
	GetMessages(mailbox model.Mailbox, from, to uint32) ([]model.Message, error)
	GetMessage(mailbox model.Mailbox, uid uint32) (model.Message, error)
	GetTotalMessages(mailbox model.Mailbox) (uint32, error)
}

type MessageWriterProvider interface {
	CreateMessage(mailbox model.Mailbox, message model.Message, body model.MessageBody) error
	DeleteMessage(mailbox model.Mailbox, uid uint32) error
}

type MessageProviderSqlite struct {
	account model.Account
	db      *sql.DB
}

func NewMessageProviderSqlite(account model.Account, db *sql.DB) *MessageProviderSqlite {
	return &MessageProviderSqlite{account: account, db: db}
}

func (m *MessageProviderSqlite) GetMessagesUids(mailbox model.Mailbox) ([]uint32, error) {
	rows, err := m.db.Query(`SELECT m.uid FROM messages m WHERE m.email = $1 and m.mailbox = $2`,
		m.account.Email, mailbox.Name)
	if err != nil {
		return nil, err
	}
	uids := make([]uint32, 0)
	for rows.Next() {
		var uid uint32
		err := rows.Scan(&uid)
		if err != nil {
			return nil, err
		}
		uids = append(uids, uid)
	}
	return uids, nil
}

func (m *MessageProviderSqlite) GetMessages(mailbox model.Mailbox, from, to uint32) ([]model.Message, error) {
	rows, err := m.db.Query(`SELECT m.id, m.uid, m.subject, mf."from", mt."to" , mc.cc , mb.bcc, mr.reply_to, ms.sender, m."date", m.in_reply_to
		FROM messages m LEFT JOIN messages_from mf ON m.uid = mf.uid AND m.email = mf.email AND m.mailbox = mf.mailbox
		LEFT JOIN messages_to mt ON m.uid = mt.uid AND m.email = mt.email AND m.mailbox = mt.mailbox
		LEFT JOIN messages_cc mc ON m.uid = mc.uid AND m.email = mc.email AND m.mailbox = mc.mailbox
		LEFT JOIN messages_bcc mb ON m.uid = mb.uid AND m.email = mb.email AND m.mailbox = mb.mailbox
		LEFT JOIN messages_sender ms ON m.uid = ms.uid AND m.email = ms.email AND m.mailbox = ms.mailbox
		LEFT JOIN messages_reply_to mr ON m.uid = mr.uid AND m.email = mr.email AND m.mailbox = mr.mailbox
		WHERE m.email = $1 and m.mailbox = $2 and m.uid BETWEEN $3 AND $4`, m.account.Email, mailbox.Name, from, to)
	if err != nil {
		return nil, err
	}
	messages := make([]model.Message, 0)
	froms := make(map[uint32]map[string]bool)
	tos := make(map[uint32]map[string]bool)
	ccs := make(map[uint32]map[string]bool)
	bccs := make(map[uint32]map[string]bool)
	senders := make(map[uint32]map[string]bool)
	replyTos := make(map[uint32]map[string]bool)
	var lastMessage model.Message
	first := true
	for rows.Next() {
		message := model.Message{}
		var from, to, cc, bcc, sender, replyTo sql.NullString
		err := rows.Scan(&message.Id, &message.Uid, &message.Subject, &from, &to, &cc, &bcc, &replyTo, &sender, &message.Date, &message.InReplyTo)
		if err != nil {
			return nil, err
		}
		addToMap(froms, message, from)
		addToMap(tos, message, to)
		addToMap(ccs, message, cc)
		addToMap(bccs, message, bcc)
		addToMap(replyTos, message, replyTo)
		addToMap(senders, message, sender)
		message.From = getKeys(froms[message.Uid])
		message.To = getKeys(tos[message.Uid])
		message.Cc = getKeys(ccs[message.Uid])
		message.Bcc = getKeys(bccs[message.Uid])
		message.ReplyTo = getKeys(replyTos[message.Uid])
		message.Sender = getKeys(senders[message.Uid])
		if !first && lastMessage.Uid != message.Uid {
			messages = append(messages, lastMessage)
		}
		lastMessage = message
		first = false
	}
	messages = append(messages, lastMessage)
	return messages, nil
}

func (m *MessageProviderSqlite) GetMessage(mailbox model.Mailbox, uid uint32) (model.Message, error) {
	rows, err := m.db.Query(`SELECT m.id, m.uid , m.subject, mf."from", mt."to", mc.cc, mb.bcc, ms.sender, mr.reply_to, m."date", in_reply_to FROM messages m
				LEFT JOIN messages_from mf ON m.uid = mf.uid AND m.email = mf.email AND m.mailbox = mf.mailbox
				LEFT JOIN messages_to mt ON m.uid = mt.uid AND m.email = mt.email AND m.mailbox = mt.mailbox
				LEFT JOIN messages_cc mc ON m.uid = mc.uid AND m.email = mc.email AND m.mailbox = mc.mailbox
				LEFT JOIN messages_bcc mb ON m.uid = mb.uid AND m.email = mb.email AND m.mailbox = mb.mailbox
				LEFT JOIN messages_sender ms ON m.uid = ms.uid AND m.email = ms.email AND m.mailbox = ms.mailbox
				LEFT JOIN messages_reply_to mr ON m.uid = mr.uid AND m.email = mr.email AND m.mailbox = mr.mailbox
				WHERE m.email = $1 and m.uid = $2 and m.mailbox = $3`, m.account.Email, uid, mailbox.Name)
	if err != nil {
		return model.Message{}, err
	}
	message := model.Message{}
	froms := make(map[string]bool)
	tos := make(map[string]bool)
	ccs := make(map[string]bool)
	bccs := make(map[string]bool)
	senders := make(map[string]bool)
	replyTos := make(map[string]bool)
	for rows.Next() {
		var from, to, cc, bcc, sender, replyTo sql.NullString
		err := rows.Scan(&message.Id, &message.Uid, &message.Subject, &from, &to, &cc, &bcc, &sender, &replyTo, &message.Date, &message.InReplyTo)
		if err != nil {
			return model.Message{}, err
		}
		addIfNotEmpty(from, froms)
		addIfNotEmpty(to, tos)
		addIfNotEmpty(cc, ccs)
		addIfNotEmpty(bcc, bccs)
		addIfNotEmpty(sender, senders)
		addIfNotEmpty(replyTo, replyTos)
	}
	message.From = getKeys(froms)
	message.To = getKeys(tos)
	message.Cc = getKeys(ccs)
	message.Bcc = getKeys(bccs)
	message.Sender = getKeys(senders)
	message.ReplyTo = getKeys(replyTos)
	return message, nil
}

func (m *MessageProviderSqlite) GetTotalMessages(mailbox model.Mailbox) (uint32, error) {
	row := m.db.QueryRow("SELECT count(*) FROM messages WHERE email = $1 and mailbox = $2", m.account.Email, mailbox.Name)
	if row.Err() != nil {
		return 0, row.Err()
	}
	var count uint32
	err := row.Scan(&count)
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (m *MessageProviderSqlite) CreateMessage(mailbox model.Mailbox, message model.Message, _ model.MessageBody) error {
	ctx := context.Background()
	tx, err := m.db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	_, err = tx.Exec("INSERT INTO messages (uid, id, subject, date, in_reply_to, email, mailbox) VALUES ($1, $2, $3, $4, $5, $6, $7)",
		message.Uid, message.Id, message.Subject, message.Date, message.InReplyTo, m.account.Email, mailbox.Name)
	if err != nil {
		defer tx.Rollback()
		return err
	}

	//TODO evaluate to avoid for in favor of INSERT multi VALUES

	for _, from := range message.From {
		err = m.insertReference(tx, message.Uid, mailbox, "from", from)
		if err != nil {
			return err
		}
	}
	for _, to := range message.To {
		err = m.insertReference(tx, message.Uid, mailbox, "to", to)
		if err != nil {
			return err
		}
	}
	for _, cc := range message.Cc {
		err = m.insertReference(tx, message.Uid, mailbox, "cc", cc)
		if err != nil {
			return err
		}
	}
	for _, bcc := range message.Bcc {
		err = m.insertReference(tx, message.Uid, mailbox, "bcc", bcc)
		if err != nil {
			return err
		}
	}
	for _, sender := range message.Sender {
		err = m.insertReference(tx, message.Uid, mailbox, "sender", sender)
		if err != nil {
			return err
		}
	}
	for _, replyTo := range message.ReplyTo {
		err := m.insertReference(tx, message.Uid, mailbox, "reply_to", replyTo)
		if err != nil {
			return err
		}
	}
	err = tx.Commit()
	if err != nil {
		return err
	}
	return nil
}

func (m *MessageProviderSqlite) DeleteMessage(_ model.Mailbox, uid uint32) error {
	_, err := m.db.Exec("DELETE FROM messages WHERE email = $1 and uid = $2", m.account.Email, uid)
	return err
}

func (m *MessageProviderSqlite) insertReference(tx *sql.Tx, uid uint32, mailbox model.Mailbox, field, fieldValue string) error {
	_, err := tx.Exec(fmt.Sprintf(`INSERT INTO messages_%s (uid, mailbox, email, "%s") VALUES ($1, $2, $3, $4)`, field, field),
		uid, mailbox.Name, m.account.Email, fieldValue)
	if err != nil {
		err := tx.Rollback()
		if err != nil {
			return err
		}
		return err
	}
	return nil
}

func getKeys(mapz map[string]bool) []string {
	keys := make([]string, len(mapz))
	i := 0
	for k, _ := range mapz {
		keys[i] = k
		i++
	}
	return keys
}

func (m *MessageProviderSqlite) getRelation(uid uint32, mailbox model.Mailbox, field string) ([]string, error) {
	rows, err := m.db.Query(fmt.Sprintf(`SELECT "%s" from messages_%s WHERE uid = $1 AND email = $2 AND mailbox = $3`, field, field), uid, m.account.Email, mailbox.Name)
	if err != nil {
		return nil, err
	}
	results := make([]string, 0)
	for rows.Next() {
		var res string
		err := rows.Scan(&res)
		if err != nil {
			return nil, err
		}
		results = append(results, res)
	}
	return results, nil
}

func addToMap(mapz map[uint32]map[string]bool, message model.Message, field sql.NullString) {
	if field.String == "" {
		return
	}
	if _, exists := mapz[message.Uid]; !exists {
		mapz[message.Uid] = make(map[string]bool)
	}
	mapz[message.Uid][field.String] = true
}

func addIfNotEmpty(field sql.NullString, mapz map[string]bool) {
	if field.String != "" {
		mapz[field.String] = true
	}
}
