package provider

import (
	"fmt"
	"github.com/emersion/go-imap"
	"github.com/emersion/go-imap/client"
	"github.com/emersion/go-message/mail"
	"github.com/emersion/go-sasl"
	"github.com/emersion/go-smtp"
	"github.com/sirupsen/logrus"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"io"
	"io/ioutil"
	"strings"
)

type MailProvider interface {
	Login() error
	Logout() error
}

type MailProviderImapSmtp struct {
	imapAccount model.Account
	smtpAccount model.Account
	client      *client.Client
}

func NewMailProviderImap(account model.Account, enableTls bool) (*MailProviderImapSmtp, error) {
	host := fmt.Sprintf("%s:%d", account.Host, account.Port)
	var err error
	var c *client.Client
	if enableTls {
		c, err = client.DialTLS(host, nil)
	} else {
		c, err = client.Dial(host)
	}
	if err != nil {
		return nil, err
	}
	return &MailProviderImapSmtp{imapAccount: account, client: c}, nil
}

/*Mail provider*/

func (m *MailProviderImapSmtp) Logout() error {
	return m.client.Logout()
}

func (m *MailProviderImapSmtp) Login() error {
	return m.client.Login(m.imapAccount.Username, m.imapAccount.Password)
}

/*Attachment reader provider*/

func (m *MailProviderImapSmtp) GetMessageAttachmentsNames(mailbox model.Mailbox, uid uint32) ([]string, error) {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return nil, err
	}
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)

	// GetMessage the whole message body
	section := &imap.BodySectionName{}
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 1)
	go func() {
		if err := m.client.UidFetch(seqSet, items, messages); err != nil {
			logrus.Error(err)
		}
	}()

	msg := <-messages
	if msg == nil {
		return nil, fmt.Errorf("empty body")
	}

	r := msg.GetBody(section)
	if r == nil {
		return nil, fmt.Errorf("empty body")
	}

	// CreateMessage a new mail reader
	mr, err := mail.CreateReader(r)
	if err != nil {
		return nil, err
	}

	// Process each message's part
	parts := make([]string, 0)
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		switch h := p.Header.(type) {
		case *mail.AttachmentHeader:
			// This is an attachment
			fields := h.Fields()
			for fields.Next() {
				fmt.Println("TEXT")
				fmt.Printf(fields.Text())
				fmt.Println()
				fmt.Println()
				fmt.Println("HEADER")
				fmt.Printf(fields.Value())
				fmt.Println()
			}
			filename, _ := h.Filename()
			parts = append(parts, filename)
		}
	}
	return parts, nil
}

func (m *MailProviderImapSmtp) GetMessageAttachment(mailbox model.Mailbox, uid uint32, attachmentName string) (model.Attachment, error) {
	panic("implement me")
}

/*MessageBody provider*/

func (m *MailProviderImapSmtp) GetMessageBody(mailbox model.Mailbox, uid uint32) (model.MessageBody, error) {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return model.MessageBody{}, err
	}
	seqSet := new(imap.SeqSet)
	seqSet.AddNum(uid)

	// GetMessage the whole message body
	section := &imap.BodySectionName{}
	items := []imap.FetchItem{section.FetchItem()}

	messages := make(chan *imap.Message, 1)
	go func() {
		if err := m.client.UidFetch(seqSet, items, messages); err != nil {
			logrus.Error(err)
		}
	}()

	msg := <-messages
	if msg == nil {
		return model.MessageBody{}, fmt.Errorf("empty body")
	}

	r := msg.GetBody(section)
	if r == nil {
		return model.MessageBody{}, fmt.Errorf("empty body")
	}

	// CreateMessage a new mail reader
	mr, err := mail.CreateReader(r)
	if err != nil {
		return model.MessageBody{}, err
	}

	// Process each message's part
	parts := make([]string, 0)
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		} else if err != nil {
			return model.MessageBody{}, err
		}

		switch h := p.Header.(type) {
		case *mail.InlineHeader:
			// This is the message's text (can be plain-text or HTML)
			b, err := ioutil.ReadAll(p.Body)
			if err != nil {
				return model.MessageBody{}, err
			}
			parts = append(parts, string(b))
			h.Len()
		}
	}
	return model.MessageBody{Text: strings.Join(parts, "\n")}, nil
}

/*Mailbox Reader provider*/

func (m *MailProviderImapSmtp) GetMailboxes() ([]model.Mailbox, error) {
	mailboxes := make(chan *imap.MailboxInfo, 10)
	done := make(chan error, 1)
	go func() {
		done <- m.client.List("", "*", mailboxes)
	}()
	mBoxesNames := make([]model.Mailbox, 0)
	for m := range mailboxes {
		mBoxesNames = append(mBoxesNames, model.Mailbox{Name: m.Name})
	}
	if err := <-done; err != nil {
		return nil, err
	}
	return mBoxesNames, nil
}

/*Message Reader provider*/

func (m *MailProviderImapSmtp) GetMessagesUids(mailbox model.Mailbox) ([]uint32, error) {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return nil, err
	}

	seqset := new(imap.SeqSet)

	uids, err := m.client.Search(&imap.SearchCriteria{
		Uid: seqset,
	})
	if err != nil {
		return nil, err
	}
	seqset.AddNum(uids...)

	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- m.client.UidFetch(seqset, []imap.FetchItem{imap.FetchUid}, messages)
	}()

	msgUids := make([]uint32, 0)
	for msg := range messages {
		msgUids = append(msgUids, msg.Uid)
	}

	if err := <-done; err != nil {
		return nil, err
	}
	return msgUids, nil
}

func (m *MailProviderImapSmtp) GetMessages(mailbox model.Mailbox, from, to uint32) ([]model.Message, error) {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return nil, err
	}

	seqset := new(imap.SeqSet)
	seqset.AddRange(from, to)

	uids, err := m.client.Search(&imap.SearchCriteria{
		Uid: seqset,
	})
	if err != nil {
		return nil, err
	}

	seqset.AddNum(uids...)

	messages := make(chan *imap.Message, 10)
	done := make(chan error, 1)
	go func() {
		done <- m.client.UidFetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
	}()

	msgs := make([]model.Message, 0)
	for msg := range messages {
		msgs = append(msgs, getMessage(msg))
	}

	if err := <-done; err != nil {
		return nil, err
	}
	return msgs, nil
}

func (m *MailProviderImapSmtp) GetMessage(mailbox model.Mailbox, uid uint32) (model.Message, error) {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return model.Message{}, err
	}

	seqset := new(imap.SeqSet)
	seqset.AddNum(uid)

	messages := make(chan *imap.Message, 1)
	done := make(chan error, 1)
	go func() {
		done <- m.client.UidFetch(seqset, []imap.FetchItem{imap.FetchEnvelope}, messages)
	}()

	var message model.Message
	for msg := range messages {
		message = getMessage(msg)
		break
	}

	if err := <-done; err != nil {
		return model.Message{}, err
	}
	return message, nil
}

func (m *MailProviderImapSmtp) GetTotalMessages(mailbox model.Mailbox) (uint32, error) {
	currentMBox := m.client.Mailbox()
	if currentMBox != nil && currentMBox.Name == mailbox.Name {
		return currentMBox.Messages, nil
	}
	mbox, err := m.selectMailBox(mailbox)
	if err != nil {
		return 0, err
	}
	return mbox.Messages, nil
}

/*Message Writer provider*/

func (m *MailProviderImapSmtp) CreateMessage(_ model.Mailbox, message model.Message, body model.MessageBody) error {
	// Set up authentication information.
	auth := sasl.NewPlainClient("", m.smtpAccount.Username, m.smtpAccount.Password)

	// Connect to the server, authenticate, set the sender and recipient,
	// and send the email all in one step.
	msg := strings.NewReader(body.Text)
	host := fmt.Sprintf("%s:%d", m.smtpAccount.Host, m.smtpAccount.Port)
	return smtp.SendMail(host, auth, m.smtpAccount.Email, message.To, msg)
}

func (m *MailProviderImapSmtp) DeleteMessage(mailbox model.Mailbox, uid uint32) error {
	_, err := m.selectMailBox(mailbox)
	if err != nil {
		return err
	}

	seqset := new(imap.SeqSet)
	seqset.AddNum(uid)

	// First mark the message as deleted
	item := imap.FormatFlagsOp(imap.AddFlags, true)
	flags := []interface{}{imap.DeletedFlag}
	if err := m.client.Store(seqset, item, flags, nil); err != nil {
		return err
	}

	// Then delete it
	return m.client.Expunge(nil)
}

func (m *MailProviderImapSmtp) selectMailBox(mailbox model.Mailbox) (*imap.MailboxStatus, error) {
	mbox := m.client.Mailbox()
	if mbox != nil && mbox.Name == mailbox.Name {
		return mbox, nil
	}
	var err error
	mbox, err = m.client.Select(mailbox.Name, false)
	if err != nil {
		return nil, err
	}
	return mbox, nil
}

func getMessage(msg *imap.Message) model.Message {
	envelope := msg.Envelope
	return model.Message{
		Uid:       msg.Uid,
		Id:        envelope.MessageId,
		Subject:   envelope.Subject,
		Cc:        toString(envelope.Cc),
		Bcc:       toString(envelope.Bcc),
		From:      toString(envelope.From),
		To:        toString(envelope.To),
		Date:      envelope.Date,
		ReplyTo:   toString(envelope.ReplyTo),
		Sender:    toString(envelope.Sender),
		InReplyTo: envelope.InReplyTo,
	}
}

func toString(cc []*imap.Address) []string {
	mails := make([]string, len(cc))
	for i, address := range cc {
		if address.PersonalName != "" {
			mails[i] = fmt.Sprintf("%s (%s)", address.PersonalName, address.Address())
		} else {
			mails[i] = address.Address()
		}
	}
	return mails
}
