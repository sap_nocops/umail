package provider

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"testing"
)

func TestAccountProviderSqlite_Create(t *testing.T) {
	db := setupAccount(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	type args struct {
		account model.Account
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		{
			name: "should create imapAccount",
			fields: fields{
				db: db,
			},
			args: args{
				account: model.Account{
					Host:     "imap.mail.com",
					Port:     993,
					Email:    "sap@test.com",
					Password: "passwd",
					Username: "sap@test.com",
				},
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			args: args{
				account: model.Account{
					Host:     "imap.mail.com",
					Port:     993,
					Email:    "sap@test.com",
					Password: "passwd",
					Username: "sap@test.com",
				},
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewAccountProviderSqlite(tt.fields.db)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			got, err := p.Create(tt.args.account)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				require.Equal(t, tt.want, got)
				assertCount(t, db, 1, "accounts")
			}
		})
	}
}

func TestAccountProviderSqlite_Delete(t *testing.T) {
	db := setupAccount(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should delete imapAccount",
			fields: fields{
				db: db,
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewAccountProviderSqlite(db)
			id, err := p.Create(model.Account{
				Email:    "sap@test.com",
				Password: "passwd",
			})
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			if err := p.Delete(id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				assertCount(t, db, 0, "accounts")
			}
		})
	}
}

func TestAccountProviderSqlite_Get(t *testing.T) {
	db := setupAccount(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	tests := []struct {
		name    string
		fields  fields
		want    model.Account
		wantErr bool
	}{
		{
			name: "should get imapAccount",
			fields: fields{
				db: db,
			},
			want: model.Account{
				Host:     "imap.mail.com",
				Port:     993,
				Email:    "sap@test.com",
				Password: "pwd",
				Username: "sap@test.com",
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			want:    model.Account{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewAccountProviderSqlite(db)
			id, err := p.Create(tt.want)
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			got, err := p.Get(id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestAccountProviderSqlite_Update(t *testing.T) {
	db := setupAccount(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	type args struct {
		delta model.Account
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should update imapAccount",
			fields: fields{
				db: db,
			},
			args: args{
				delta: model.Account{
					Host:     "imap.mail.com",
					Port:     993,
					Email:    "sap@test.com",
					Password: "pwd",
					Username: "sap@test.com",
				},
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			args: args{
				delta: model.Account{
					Host:     "imap.mail.com",
					Port:     993,
					Email:    "sap@test.com",
					Password: "pwd",
					Username: "sap@test.com",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewAccountProviderSqlite(db)
			id, err := p.Create(model.Account{
				Email:    "sap@mail.com",
				Password: "zerbino",
				Host:     "imap.mail.com",
				Port:     993,
				Username: "sap@mail.com",
			})
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			if err := p.Update(id, tt.args.delta); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				var email, username, password, host string
				var port int
				row := db.QueryRow("SELECT email, username, password, host, port FROM accounts WHERE id = $1", id)
				require.NoError(t, row.Err())
				require.NoError(t, row.Scan(&email, &username, &password, &host, &port))
				require.Equal(t, "sap@mail.com", email)
				require.Equal(t, "sap@mail.com", username)
				require.Equal(t, tt.args.delta.Password, password)
				require.Equal(t, tt.args.delta.Host, host)
				require.Equal(t, tt.args.delta.Port, port)
			}
		})
	}
}

func TestAccountProviderSqlite_GetAll(t *testing.T) {
	db := setupAccount(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Account
		wantErr bool
	}{
		{
			name: "should get all accounts",
			fields: fields{
				db: db,
			},
			want: []model.Account{
				{
					Host:     "imap.test.com",
					Port:     222,
					Email:    "test@test.com",
					Password: "aaa",
					Username: "test",
				},
				{
					Host:     "imap.umail.io",
					Port:     3456,
					Email:    "sap@umail.io",
					Password: "*****",
					Username: "sap",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			a := &AccountProviderSqlite{
				db: tt.fields.db,
			}
			for _, account := range tt.want {
				_, err := a.Create(account)
				require.NoError(t, err)
			}
			got, err := a.GetAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func setupAccount(t *testing.T) *sql.DB {
	db, err := sql.Open("sqlite3", "test_data/test.db")
	if err != nil {
		require.NoError(t, err)
	}
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS accounts (id INTEGER PRIMARY KEY AUTOINCREMENT, email VARCHAR(255), username VARCHAR(255), password VARCHAR(255), host VARCHAR(255), port INT)")
	require.NoError(t, err)
	return db
}
