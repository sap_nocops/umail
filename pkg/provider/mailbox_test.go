package provider

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"testing"
)

func TestMailboxProviderSqlite_GetMailboxes(t *testing.T) {
	db := setupMailboxes(t)
	defer tearDown(t, db)

	_, err := db.Exec("INSERT INTO mailboxes (mailbox, email) VALUES ('INBOX', 'sap@mail.com'), ('SENT', 'sap@mail.com'), ('TRASH', 'sap@mail.com')")
	require.NoError(t, err)

	type fields struct {
		db      *sql.DB
		account model.Account
	}
	tests := []struct {
		name    string
		fields  fields
		want    []model.Mailbox
		wantErr bool
	}{
		{
			name: "shouldGetMailboxes",
			fields: fields{
				db: db,
				account: model.Account{
					Email: "sap@mail.com",
				},
			},
			want: []model.Mailbox{
				{
					Name: "INBOX",
				},
				{
					Name: "SENT",
				},
				{
					Name: "TRASH",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := NewMailboxProviderSqlite(tt.fields.db, tt.fields.account)
			got, err := m.GetMailboxes()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMailboxes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMailboxProviderSqlite_SaveMailboxes(t *testing.T) {
	db := setupMailboxes(t)
	defer tearDown(t, db)

	type fields struct {
		db      *sql.DB
		account model.Account
	}
	type args struct {
		mailboxes []model.Mailbox
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "shouldSaveMailboxes",
			fields: fields{
				db: db,
				account: model.Account{
					Email: "test@mail.com",
				},
			},
			args: args{
				mailboxes: []model.Mailbox{
					{
						Name: "INBOX",
					},
					{
						Name: "OUTBOX",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "shouldUpsertMailboxes",
			fields: fields{
				db: db,
				account: model.Account{
					Email: "test@mail.com",
				},
			},
			args: args{
				mailboxes: []model.Mailbox{
					{
						Name: "INBOX",
					},
					{
						Name: "INBOX",
					},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := NewMailboxProviderSqlite(tt.fields.db, tt.fields.account)
			if err := m.SaveMailboxes(tt.args.mailboxes...); (err != nil) != tt.wantErr {
				t.Errorf("SaveMailboxes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				mailboxes, err := m.GetMailboxes()
				require.NoError(t, err)
				require.Equal(t, distinct(tt.args.mailboxes), mailboxes)
			}
		})
	}
}

func distinct(mailboxes []model.Mailbox) []model.Mailbox {
	set := make(map[string]bool)
	for _, mailbox := range mailboxes {
		set[mailbox.Name] = true
	}
	distinct := make([]model.Mailbox, len(set))
	i := 0
	for k, _ := range set {
		distinct[i] = model.Mailbox{
			Name: k,
		}
		i++
	}
	return distinct
}

func setupMailboxes(t *testing.T) *sql.DB {
	db, err := sql.Open("sqlite3", "test_data/test.db")
	if err != nil {
		require.NoError(t, err)
	}
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS mailboxes (mailbox VARCHAR(255), email VARCHAR(255), PRIMARY KEY (mailbox, email))")
	require.NoError(t, err)
	return db
}
