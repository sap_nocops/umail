package provider

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"testing"
	"time"
)

func TestMessageProviderSqlite_CreateMessage(t *testing.T) {
	db := setupMessages(t)
	defer tearDown(t, db)

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		message model.Message
		body    model.MessageBody
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should create message",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				message: message(1, time.Now()),
				body: model.MessageBody{
					Text: "all right!",
				},
			},
			wantErr: false,
		},
		{
			name: "should create message2",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				message: message2(1, time.Now()),
				body: model.MessageBody{
					Text: "all right!",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			if err := m.CreateMessage(tt.args.mailbox, tt.args.message, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("CreateMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				require.Equal(t, 1, getCount(t, db, "SELECT count(*) from messages"))
				require.Equal(t, len(tt.args.message.From), getCount(t, db, "SELECT count(*) from messages_from"))
				require.Equal(t, len(tt.args.message.To), getCount(t, db, "SELECT count(*) from messages_to"))
				require.Equal(t, len(tt.args.message.Cc), getCount(t, db, "SELECT count(*) from messages_cc"))
				require.Equal(t, len(tt.args.message.Bcc), getCount(t, db, "SELECT count(*) from messages_bcc"))
				require.Equal(t, len(tt.args.message.ReplyTo), getCount(t, db, "SELECT count(*) from messages_reply_to"))
				require.Equal(t, len(tt.args.message.Sender), getCount(t, db, "SELECT count(*) from messages_sender"))
			}
			_, err := db.Exec("DELETE FROM messages")
			require.NoError(t, err)
		})
	}
}

func TestMessageProviderSqlite_DeleteMessage(t *testing.T) {
	db := setupMessages(t)
	defer tearDown(t, db)

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		uid     uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should delete message",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				uid: 1,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			require.NoError(t, m.CreateMessage(tt.args.mailbox, message(1, time.Now()), model.MessageBody{}))
			if err := m.DeleteMessage(tt.args.mailbox, tt.args.uid); (err != nil) != tt.wantErr {
				t.Errorf("DeleteMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_from"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_to"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_cc"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_bcc"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_reply_to"))
				require.Equal(t, 0, getCount(t, db, "SELECT count(*) from messages_sender"))
			}
		})
	}
}

func TestMessageProviderSqlite_GetMessage(t *testing.T) {
	db := setupMessages(t)
	defer tearDown(t, db)

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		uid     uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Message
		wantErr bool
	}{
		{
			name: "should get message",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{Name: "INBOX"},
				uid:     1,
			},
			want:    message(1, time.Now()),
			wantErr: false,
		},
		{
			name: "should get message2",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{Name: "INBOX"},
				uid:     1,
			},
			want:    message2(1, time.Now()),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			require.NoError(t, m.CreateMessage(tt.args.mailbox, tt.want, model.MessageBody{}))
			got, err := m.GetMessage(tt.args.mailbox, tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			compareMessage(t, tt.want, got)
			_, err = db.Exec("DELETE FROM messages")
			require.NoError(t, err)
		})
	}
}

func TestMessageProviderSqlite_GetMessages(t *testing.T) {
	db := setupMessages(t)
	defer tearDown(t, db)
	now := time.Now()

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		from    uint32
		to      uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []model.Message
		wantErr bool
	}{
		{
			name: "should get messages",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				from: 1,
				to:   2,
			},
			want: []model.Message{
				message(1, now),
				message(2, now),
			},
			wantErr: false,
		},
		{
			name: "should get message",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				from: 1,
				to:   2,
			},
			want: []model.Message{
				message(1, now),
			},
			wantErr: false,
		},
		{
			name: "should get message2",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				from: 1,
				to:   1,
			},
			want: []model.Message{
				message2(1, now),
			},
			wantErr: false,
		},
		{
			name: "should get message and message2",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				from: 1,
				to:   2,
			},
			want: []model.Message{
				message(1, now),
				message2(2, now),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			for _, mes := range tt.want {
				require.NoError(t, m.CreateMessage(tt.args.mailbox, mes, model.MessageBody{}))
			}
			got, err := m.GetMessages(tt.args.mailbox, tt.args.from, tt.args.to)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessages() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, len(tt.want), len(got))
			for i, want := range tt.want {
				compareMessage(t, want, got[i])
			}
			_, err = db.Exec("DELETE FROM messages")
			require.NoError(t, err)
		})
	}
}

func TestMessageProviderSqlite_GetTotalMessages(t *testing.T) {
	db := setupMessages(t)
	defer tearDown(t, db)
	now := time.Now()

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    uint32
		wantErr bool
	}{
		{
			name: "should get messages count",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
			},
			want:    2,
			wantErr: false,
		},
		{
			name: "should get no messages count",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
			},
			want:    0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			for i := uint32(0); i < tt.want; i++ {
				require.NoError(t, m.CreateMessage(tt.args.mailbox, message(i+1, now), model.MessageBody{}))
			}
			got, err := m.GetTotalMessages(tt.args.mailbox)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTotalMessages() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
			_, err = db.Exec("DELETE FROM messages")
			require.NoError(t, err)
		})
	}
}

func setupMessages(t *testing.T) *sql.DB {
	db, err := sql.Open("sqlite3", "test_data/test.db")
	if err != nil {
		require.NoError(t, err)
	}
	_, err = db.Exec("PRAGMA foreign_keys = ON")
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages (uid BIGINT, id VARCHAR(100), subject VARCHAR(100),
    "date" DATETIME, email VARCHAR(100), mailbox VARCHAR(100), in_reply_to VARCHAR(100),
    from_id BIGINT, to_id BIGINT, cc_id BIGINT, bcc_id BIGINT, sender_id BIGINT, reply_to_id BIGINT, 
    PRIMARY KEY (uid, mailbox, email))`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_from (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), "from" VARCHAR(100),
    PRIMARY KEY (uid, mailbox, email, "from"),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_to (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), "to" VARCHAR(100),
    PRIMARY KEY (uid, mailbox, email, "to"),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_cc (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), cc VARCHAR(100),
		PRIMARY KEY (uid, mailbox, email, cc),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_bcc (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), bcc VARCHAR(100),
    PRIMARY KEY (uid, mailbox, email, bcc),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_sender (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), sender VARCHAR(100),
    PRIMARY KEY (uid, mailbox, email, sender),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	_, err = db.Exec(`CREATE TABLE IF NOT EXISTS messages_reply_to (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), reply_to VARCHAR(100),
    PRIMARY KEY (uid, mailbox, email, reply_to),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	return db
}

func message(uid uint32, t time.Time) model.Message {
	return model.Message{
		Uid:       uid,
		Id:        "aaaa-bbbb-cccc-dddd",
		Subject:   "test",
		From:      []string{"plato@mail.gr"},
		To:        []string{"a@a.com", "b@b.com"},
		Cc:        []string{"c@c.com", "d@d.com"},
		Bcc:       []string{"e@e.com", "f@f.com"},
		ReplyTo:   []string{"a@a.com", "b@b.com"},
		Sender:    []string{"plato@mail.gr"},
		Date:      t,
		InReplyTo: "a@a.com",
	}
}

func message2(uid uint32, t time.Time) model.Message {
	return model.Message{
		Uid:       uid,
		Id:        "aaaa-bbbb-cccc-dddd",
		Subject:   "test",
		From:      []string{"plato@mail.gr"},
		To:        []string{"a@a.com"},
		Sender:    []string{"plato@mail.gr"},
		Date:      t,
		InReplyTo: "",
	}
}

func getCount(t *testing.T, db *sql.DB, query string) int {
	row := db.QueryRow(query)
	require.NoError(t, row.Err())
	var cnt int
	require.NoError(t, row.Scan(&cnt))
	return cnt
}

func compareMessage(t *testing.T, expected, actual model.Message) {
	require.Equal(t, expected.Uid, actual.Uid)
	require.Equal(t, expected.Id, actual.Id)
	require.ElementsMatch(t, expected.ReplyTo, actual.ReplyTo)
	require.ElementsMatch(t, expected.Sender, actual.Sender)
	require.ElementsMatch(t, expected.Cc, actual.Cc)
	require.ElementsMatch(t, expected.Bcc, actual.Bcc)
	require.ElementsMatch(t, expected.From, actual.From)
	require.ElementsMatch(t, expected.To, actual.To)
	require.Equal(t, expected.Subject, actual.Subject)
	require.Equal(t, expected.InReplyTo, actual.InReplyTo)
	require.True(t, expected.Date.Equal(actual.Date))
}
