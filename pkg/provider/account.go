package provider

import (
	"database/sql"
	"gitlab.com/sap_nocops/umail/pkg/model"
)

type AccountProvider interface {
	GetAll() ([]model.Account, error)
	Get(id int64) (model.Account, error)
	Create(profile model.Account) (int64, error)
	Update(id int64, delta model.Account) error
	Delete(id int64) error
}

type AccountProviderSqlite struct {
	db *sql.DB
}

func NewAccountProviderSqlite(db *sql.DB) *AccountProviderSqlite {
	return &AccountProviderSqlite{
		db: db,
	}
}

func (a *AccountProviderSqlite) GetAll() ([]model.Account, error) {
	rows, err := a.db.Query("SELECT email, username, password, host, port FROM accounts")
	if err != nil {
		return nil, err
	}
	accounts := make([]model.Account, 0)
	for rows.Next() {
		var account model.Account
		err := rows.Scan(&account.Email, &account.Username, &account.Password, &account.Host, &account.Port)
		if err != nil {
			return nil, err
		}
		accounts = append(accounts, account)
	}
	return accounts, nil
}

func (a *AccountProviderSqlite) Get(id int64) (model.Account, error) {
	row := a.db.QueryRow("SELECT email, username, password, host, port FROM accounts WHERE id = $1", id)
	if row.Err() != nil {
		return model.Account{}, row.Err()
	}
	account := model.Account{}
	err := row.Scan(&account.Email, &account.Username, &account.Password, &account.Host, &account.Port)
	if err != nil {
		return model.Account{}, err
	}
	return account, nil
}

func (a *AccountProviderSqlite) Create(account model.Account) (int64, error) {
	exec, err := a.db.Exec("INSERT INTO accounts (email, username, password, host, port) VALUES ($1, $2, $3, $4, $5)",
		account.Email, account.Username, account.Password, account.Host, account.Port)
	if err != nil {
		return 0, err
	}
	return exec.LastInsertId()
}

func (a *AccountProviderSqlite) Update(id int64, delta model.Account) error {
	_, err := a.db.Exec("UPDATE accounts SET password = $1, host = $2, port = $3 WHERE id = $4",
		delta.Password, delta.Host, delta.Port, id)
	return err
}

func (a *AccountProviderSqlite) Delete(id int64) error {
	_, err := a.db.Exec("DELETE FROM accounts WHERE id = $1", id)
	return err
}
