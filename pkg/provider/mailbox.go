package provider

import (
	"database/sql"
	"fmt"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"strings"
)

type MailboxReaderProvider interface {
	GetMailboxes() ([]model.Mailbox, error)
}

type MailboxWriterProvider interface {
	SaveMailboxes(mailboxes ...model.Mailbox) error
}

type MailboxProviderSqlite struct {
	db *sql.DB
	account model.Account
}

func NewMailboxProviderSqlite(db *sql.DB, account model.Account) *MailboxProviderSqlite {
	return &MailboxProviderSqlite{db: db, account: account}
}

func (m *MailboxProviderSqlite) GetMailboxes() ([]model.Mailbox, error) {
	rows, err := m.db.Query("SELECT mailbox FROM mailboxes WHERE email = $1", m.account.Email)
	if err != nil {
		return nil, err
	}
	mailboxes := make([]model.Mailbox, 0)
	for rows.Next() {
		var mailbox string
		err = rows.Scan(&mailbox)
		if err != nil {
			return nil, err
		}
		mailboxes = append(mailboxes, model.Mailbox{Name: mailbox})
	}
	return mailboxes, nil
}

func (m *MailboxProviderSqlite) SaveMailboxes(mailboxes ...model.Mailbox) error {
	values := make([]string, len(mailboxes))
	for i, mailbox := range mailboxes {
		values[i] = fmt.Sprintf("('%s','%s')", mailbox.Name, m.account.Email)
	}
	_, err := m.db.Exec(fmt.Sprintf("INSERT INTO mailboxes (mailbox, email) VALUES %s ON CONFLICT DO NOTHING", strings.Join(values, ",")))
	return err
}
