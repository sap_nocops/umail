package provider

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"os"
	"testing"
)

func TestProfileProviderSqlite_Create(t *testing.T) {
	db := setupProfile(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	type args struct {
		profile model.Profile
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int64
		wantErr bool
	}{
		{
			name: "should create profile",
			fields: fields{
				db: db,
			},
			args: args{
				profile: model.Profile{
					Name: "Plato",
				},
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			args: args{
				profile: model.Profile{
					Name: "Plato",
				},
			},
			want:    0,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewProfileProviderSqlite(tt.fields.db)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			got, err := p.Create(tt.args.profile)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				require.Equal(t, tt.want, got)
				assertCount(t, db, 1, "profiles")
			}
		})
	}
}

func TestProfileProviderSqlite_Delete(t *testing.T) {
	db := setupProfile(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should delete profile",
			fields: fields{
				db: db,
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewProfileProviderSqlite(db)
			id, err := p.Create(model.Profile{
				Name: "aaa",
			})
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			if err := p.Delete(id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteMessage() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !tt.wantErr {
				assertCount(t, db, 0, "profiles")
			}
		})
	}
}

func TestProfileProviderSqlite_Get(t *testing.T) {
	db := setupProfile(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	tests := []struct {
		name    string
		fields  fields
		want    model.Profile
		wantErr bool
	}{
		{
			name: "should get profile",
			fields: fields{
				db: db,
			},
			want: model.Profile{
				Name: "antani",
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			want:    model.Profile{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewProfileProviderSqlite(db)
			id, err := p.Create(tt.want)
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			got, err := p.Get(id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestProfileProviderSqlite_Update(t *testing.T) {
	db := setupProfile(t)
	defer tearDown(t, db)
	type fields struct {
		db *sql.DB
	}
	type args struct {
		delta model.Profile
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should update profile",
			fields: fields{
				db: db,
			},
			args: args{
				delta: model.Profile{
					Name: "mailani",
				},
			},
			wantErr: false,
		},
		{
			name: "should error when db error",
			fields: fields{
				db: db,
			},
			args: args{
				delta: model.Profile{
					Name: "mailani",
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewProfileProviderSqlite(db)
			id, err := p.Create(model.Profile{
				Name: "aaaaa",
			})
			require.NoError(t, err)
			if tt.wantErr {
				require.NoError(t, db.Close())
			}
			if err := p.Update(id, tt.args.delta); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				var name string
				row := db.QueryRow("SELECT name FROM profiles WHERE id = $1", id)
				require.NoError(t, row.Err())
				require.NoError(t, row.Scan(&name))
				require.Equal(t, tt.args.delta.Name, name)
			}
		})
	}
}

func tearDown(t *testing.T, db *sql.DB) {
	require.NoError(t, db.Close())
	require.NoError(t, os.Remove("test_data/test.db"))
}

func setupProfile(t *testing.T) *sql.DB {
	db, err := sql.Open("sqlite3", "test_data/test.db")
	if err != nil {
		require.NoError(t, err)
	}
	_, err = db.Exec("CREATE TABLE IF NOT EXISTS profiles (id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(255))")
	require.NoError(t, err)
	return db
}

func assertCount(t *testing.T, db *sql.DB, n int, table string) {
	var cnt int
	row := db.QueryRow("SELECT COUNT(*) FROM " + table)
	require.NoError(t, row.Err())
	require.NoError(t, row.Scan(&cnt))
	require.Equal(t, n, cnt)
}
