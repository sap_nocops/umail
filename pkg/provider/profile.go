package provider

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"gitlab.com/sap_nocops/umail/pkg/model"
)

type ProfileProvider interface {
	Get(id int64) (model.Profile, error)
	Create(profile model.Profile) (int64, error)
	Update(id int64, delta model.Profile) error
	Delete(id int64) error
}

type ProfileProviderSqlite struct {
	db *sql.DB
}

func NewProfileProviderSqlite(db *sql.DB) *ProfileProviderSqlite {
	return &ProfileProviderSqlite{
		db: db,
	}
}

func (p *ProfileProviderSqlite) Get(id int64) (model.Profile, error) {
	row := p.db.QueryRow("SELECT name FROM profiles WHERE id = $1", id)
	if row.Err() != nil {
		return model.Profile{}, row.Err()
	}
	profile := model.Profile{}
	err := row.Scan(&profile.Name)
	if err != nil {
		return model.Profile{}, err
	}
	return profile, nil
}

func (p *ProfileProviderSqlite) Create(profile model.Profile) (int64, error) {
	exec, err := p.db.Exec("INSERT INTO profiles (name) VALUES ($1)", profile.Name)
	if err != nil {
		return 0, err
	}
	return exec.LastInsertId()
}

func (p *ProfileProviderSqlite) Update(id int64, delta model.Profile) error {
	_, err := p.db.Exec("UPDATE profiles SET name = $1 WHERE id = $2", delta.Name, id)
	return err
}

func (p *ProfileProviderSqlite) Delete(id int64) error {
	_, err := p.db.Exec("DELETE FROM profiles WHERE id = $1", id)
	return err
}
