package provider

import (
	"github.com/emersion/go-imap/backend/memory"
	"github.com/emersion/go-imap/server"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"log"
	"testing"
	"time"
)

func TestMailProviderImap_GetMessages(t *testing.T) {
	mailServer := startUpMailServer()
	defer mailServer.Close()
	type fields struct {
		account model.Account
	}
	type args struct {
		mailbox model.Mailbox
		from    uint32
		to      uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    []model.Message
	}{
		{
			name: "should get messages",
			fields: fields{
				account: getAccount(),
			},
			args: args{
				mailbox: model.Mailbox{Name: "INBOX"},
				from:    1,
				to:      50,
			},
			want: []model.Message{
				{
					Subject: "A little message, just for you",
					Uid:     0,
					Id:      "<0000000@localhost/>",
					From:    []string{"contact@example.org"},
					To:      []string{"contact@example.org"},
					Cc:      []string{},
					Bcc:     []string{},
					ReplyTo: []string{"contact@example.org"},
					Sender:  []string{"contact@example.org"},
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewMailProviderImap(tt.fields.account, false)
			require.NoError(t, err)
			err = s.Login()
			defer s.Logout()
			require.NoError(t, err)
			got, err := s.GetMessages(tt.args.mailbox, tt.args.from, tt.args.to)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessages() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, len(tt.want), len(got))
			for i := range tt.want {
				tt.want[i].Date = got[i].Date
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMailProviderImap_GetMailboxes(t *testing.T) {
	mailServer := startUpMailServer()
	defer mailServer.Close()
	type fields struct {
		account model.Account
	}
	tests := []struct {
		name    string
		fields  fields
		want    []string
		wantErr bool
	}{
		{
			name: "shuold get mailboxes",
			fields: fields{
				account: getAccount(),
			},
			want:    []string{"INBOX"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewMailProviderImap(tt.fields.account, false)
			require.NoError(t, err)
			err = s.Login()
			defer s.Logout()
			require.NoError(t, err)
			got, err := s.GetMailboxes()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMailboxes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMailProviderImap_GetTotalMessages(t *testing.T) {
	mailServer := startUpMailServer()
	defer mailServer.Close()
	type fields struct {
		account model.Account
	}
	type args struct {
		mailbox model.Mailbox
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    uint32
		wantErr bool
	}{
		{
			name: "should get total messages",
			fields: fields{
				account: getAccount(),
			},
			args: args{
				mailbox: model.Mailbox{Name: "INBOX"},
			},
			want:    1,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewMailProviderImap(tt.fields.account, false)
			require.NoError(t, err)
			err = s.Login()
			defer s.Logout()
			require.NoError(t, err)
			got, err := s.GetTotalMessages(tt.args.mailbox)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetTotalMessages() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMailProviderImap_GetMessageBody(t *testing.T) {
	mailServer := startUpMailServer()
	defer mailServer.Close()
	time.Sleep(1 * time.Second)
	type fields struct {
		account model.Account
	}
	type args struct {
		mailbox model.Mailbox
		id      uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "should get message body",
			fields: fields{
				account: getAccount(),
			},
			args: args{
				mailbox: model.Mailbox{Name: "INBOX"},
				id:      0,
			},
			want:    "antani",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s, err := NewMailProviderImap(tt.fields.account, false)
			require.NoError(t, err)
			err = s.Login()
			defer s.Logout()
			require.NoError(t, err)
			got, err := s.GetMessageBody(tt.args.mailbox, tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessageBody() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func startUpMailServer() *server.Server {
	// CreateMessage a memory backend
	be := memory.New()

	// CreateMessage a new server
	s := server.New(be)
	s.Addr = ":1143"
	s.AllowInsecureAuth = true

	log.Println("Starting IMAP server at localhost:1143")
	go func() {
		err := s.ListenAndServe()
		logrus.Errorf("cannot start server: %v", err)
	}()
	return s
}

func getAccount() model.Account {
	return model.Account{
		Host:     "localhost",
		Port:     1143,
		Email:    "localhost",
		Password: "password",
		Username: "username",
	}
}
