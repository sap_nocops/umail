package provider

import (
	"fmt"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"io/ioutil"
	"os"
)

//TODO write

type AttachmentReaderProvider interface {
	GetMessageAttachmentsNames(mailbox model.Mailbox, uid uint32) ([]string, error)
	GetMessageAttachment(mailbox model.Mailbox, uid uint32, attachmentName string) (model.Attachment, error)
}

type AttachmentProviderFs struct {
	account  model.Account
	basePath string
}

func (a *AttachmentProviderFs) GetMessageAttachments(mailbox model.Mailbox, uid uint32) ([]string, error) {
	path := fmt.Sprintf("%s/%s/%s/%d", a.basePath, a.account.Email, mailbox.Name, uid)
	_, err := os.Stat(path)
	if os.IsNotExist(err) {
		return nil, fmt.Errorf("message %d of %s does not have attachments", uid, a.account.Email)
	}
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return nil, err
	}
	attachments := make([]string, len(files))
	for i := 0; i < len(files); i++ {
		attachments[i] = files[i].Name()
	}
	return attachments, nil
}

func (a *AttachmentProviderFs) GetMessageAttachment(mailbox model.Mailbox, uid uint32, attachmentName string) (model.Attachment, error) {
	path := fmt.Sprintf("%s/%s/%s/%d/%s", a.basePath, a.account.Email, mailbox.Name, uid, attachmentName)
	attachment, err := ioutil.ReadFile(path)
	if err != nil {
		return model.Attachment{}, err
	}
	return model.Attachment{
		Filename: attachmentName,
		Body:     attachment,
	}, nil
}
