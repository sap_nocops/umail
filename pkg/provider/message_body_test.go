package provider

import (
	"database/sql"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"testing"
	"time"
)

func TestMessageBodyProviderSqlite_GetMessageBody(t *testing.T) {
	db := setupMessageBodies(t)
	defer tearDown(t, db)

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		uid     uint32
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.MessageBody
		wantErr bool
	}{
		{
			name: "should get message body",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				uid: 1,
			},
			want:    model.MessageBody{Text: "<h1>HELLO!</h1>"},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			m := &MessageBodyProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			InsertMessage(t, tt.args.uid, tt.args.mailbox, tt.fields.account, tt.fields.db)
			require.NoError(t, m.SaveMessageBody(tt.args.mailbox, tt.args.uid, tt.want))
			got, err := m.GetMessageBody(tt.args.mailbox, tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetMessageBody() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			require.Equal(t, tt.want, got)
		})
	}
}

func TestMessageBodyProviderSqlite_SaveMessageBody(t *testing.T) {
	db := setupMessageBodies(t)
	defer tearDown(t, db)

	type fields struct {
		account model.Account
		db      *sql.DB
	}
	type args struct {
		mailbox model.Mailbox
		uid     uint32
		body    model.MessageBody
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should save message body",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				db: db,
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
				uid: 1,
				body: model.MessageBody{
					Text: "<h1>HELLO!</h1>",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			InsertMessage(t, tt.args.uid, tt.args.mailbox, tt.fields.account, tt.fields.db)
			m := &MessageBodyProviderSqlite{
				account: tt.fields.account,
				db:      tt.fields.db,
			}
			if err := m.SaveMessageBody(tt.args.mailbox, tt.args.uid, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("SaveMessageBody() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			count := getCount(t, db, "SELECT COUNT(*) FROM message_bodies")
			require.Equal(t, 1, count)
		})
	}
}

func InsertMessage(t *testing.T, uid uint32, mailbox model.Mailbox, account model.Account, db *sql.DB) {
	mp := &MessageProviderSqlite{
		account: account,
		db:      db,
	}
	require.NoError(t, mp.CreateMessage(mailbox, model.Message{
		Uid:  uid,
		Date: time.Now(),
	}, model.MessageBody{}))
}

func setupMessageBodies(t *testing.T) *sql.DB {
	db := setupMessages(t)

	_, err := db.Exec(`CREATE TABLE IF NOT EXISTS message_bodies (uid BIGINT, email VARCHAR(100), mailbox VARCHAR(100), body TEXT,
    PRIMARY KEY (uid, mailbox, email),
    FOREIGN KEY (uid, mailbox, email) REFERENCES messages(uid, mailbox, email) ON DELETE CASCADE)`)
	require.NoError(t, err)
	return db
}
