package middleware

import (
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"gitlab.com/sap_nocops/umail/pkg/provider"
	"testing"
)

type ProfilerProviderMock struct {

}

func (p *ProfilerProviderMock) Get(id int64) (model.Profile, error) {
	panic("implement me")
}

func (p *ProfilerProviderMock) Create(profile model.Profile) (int64, error) {
	panic("implement me")
}

func (p *ProfilerProviderMock) Update(id int64, delta model.Profile) error {
	panic("implement me")
}

func (p *ProfilerProviderMock) Delete(id int64) error {
	panic("implement me")
}

func TestQmlUiBridge_GetProfiles(t *testing.T) {
	type fields struct {
		profileProvider provider.ProfileProvider
		accountProvider provider.AccountProvider
	}
	tests := []struct {
		name   string
		fields fields
	}{
		{
			name:   "should set profiles",
			fields: fields{
				profileProvider: ,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			q := NewQmlUiBridge(tt.fields.profileProvider, tt.fields.accountProvider)
			q.GetProfiles()

			require.Equal(t, profiles, q.Profiles)
		})
	}
}