package middleware

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"gitlab.com/sap_nocops/umail/pkg/model"
	"gitlab.com/sap_nocops/umail/pkg/provider"
	"testing"
	"time"
)

func TestImapSqliteSynchronizer_SyncMail(t *testing.T) {
	now := time.Now()
	type fields struct {
		account             model.Account
		mailProvider        provider.MailProvider
		remoteMessageReader provider.MessageReaderProvider
		localMessageReader  provider.MessageReaderProvider
		messageWriter       provider.MessageWriterProvider
		mailboxReader       provider.MailboxReaderProvider
		mailboxWriter       provider.MailboxWriterProvider
	}
	type args struct {
		mailbox model.Mailbox
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "should sync emails ADD",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				mailProvider: &MockMailProvider{},
				remoteMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {
							{
								Uid:     0,
								Id:      "0",
								Subject: "hello",
								From:    []string{"friend@mail.com"},
								To:      []string{"test@mail.com"},
								Cc:      []string{"other_friend@mail.com"},
								Sender:  []string{"friend@mail.com"},
								Date:    now,
							},
						},
					},
				},
				localMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {},
					},
				},
				messageWriter: NewMockMessageWriter(),
				mailboxReader: &MockMailboxReader{},
				mailboxWriter: NewMockMailboxWriter(),
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
			},
			wantErr: false,
		},
		{
			name: "should sync emails REMOVE",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				mailProvider: &MockMailProvider{},
				remoteMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {},
					},
				},
				localMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {
							{
								Uid:     0,
								Id:      "0",
								Subject: "hello",
								From:    []string{"friend@mail.com"},
								To:      []string{"test@mail.com"},
								Cc:      []string{"other_friend@mail.com"},
								Sender:  []string{"friend@mail.com"},
								Date:    now,
							},
						},
					},
				},
				messageWriter: NewMockMessageWriter(),
				mailboxReader: &MockMailboxReader{},
				mailboxWriter: NewMockMailboxWriter(),
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
			},
			wantErr: false,
		},
		{
			name: "should sync emails ADD and REMOVE",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				mailProvider: &MockMailProvider{},
				remoteMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {
							{
								Uid:     0,
								Id:      "0",
								Subject: "hello",
								From:    []string{"friend@mail.com"},
								To:      []string{"test@mail.com"},
								Cc:      []string{"other_friend@mail.com"},
								Sender:  []string{"friend@mail.com"},
								Date:    now,
							},
						},
					},
				},
				localMessageReader: &MockMessageReader{
					messages: map[string][]model.Message{
						"INBOX": {
							{
								Uid:     1,
								Id:      "1",
								Subject: "bye",
								From:    []string{"friend@mail.com"},
								To:      []string{"test@mail.com"},
								Cc:      []string{"other_friend@mail.com"},
								Sender:  []string{"friend@mail.com"},
								Date:    now,
							},
						},
					},
				},
				messageWriter: NewMockMessageWriter(),
				mailboxReader: &MockMailboxReader{},
				mailboxWriter: NewMockMailboxWriter(),
			},
			args: args{
				mailbox: model.Mailbox{
					Name: "INBOX",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &ImapSqliteSynchronizer{
				Account:             tt.fields.account,
				MailProvider:        tt.fields.mailProvider,
				RemoteMessageReader: tt.fields.remoteMessageReader,
				LocalMessageReader:  tt.fields.localMessageReader,
				MessageWriter:       tt.fields.messageWriter,
				MailboxReader:       tt.fields.mailboxReader,
				MailboxWriter:       tt.fields.mailboxWriter,
			}
			if err := i.SyncMail(tt.args.mailbox); (err != nil) != tt.wantErr {
				t.Errorf("SyncMail() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				lmr := tt.fields.localMessageReader.(*MockMessageReader)
				rmr := tt.fields.remoteMessageReader.(*MockMessageReader)
				toAdd, toRemove := getDiffs(getUids(tt.args.mailbox, rmr.messages), getUids(tt.args.mailbox, lmr.messages))
				mw := tt.fields.messageWriter.(*MockMessageWriter)
				require.Equal(t, getMessages(tt.args.mailbox, toAdd, rmr.messages), asMessages(mw.createdMessages[tt.args.mailbox.Name]))
				if len(toRemove) == 0 {
					require.Nil(t, mw.deletedMessages[tt.args.mailbox.Name])
					return
				}
				require.Equal(t, toRemove, mw.deletedMessages[tt.args.mailbox.Name])
			}
		})
	}
}

func TestImapSqliteSynchronizer_SyncMailBoxes(t *testing.T) {
	type fields struct {
		account       model.Account
		mailProvider  provider.MailProvider
		messageReader provider.MessageReaderProvider
		messageWriter provider.MessageWriterProvider
		mailboxReader provider.MailboxReaderProvider
		mailboxWriter provider.MailboxWriterProvider
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should Sync Mailboxes",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				mailProvider:  &MockMailProvider{},
				messageReader: &MockMessageReader{},
				messageWriter: &MockMessageWriter{},
				mailboxReader: &MockMailboxReader{
					mailboxes: []model.Mailbox{
						{Name: "INBOX"},
						{Name: "OUTBOX"},
						{Name: "TRASH"},
						{Name: "SPAM"},
					},
				},
				mailboxWriter: NewMockMailboxWriter(),
			},
			wantErr: false,
		},
		{
			name: "should error when get mailboxes error",
			fields: fields{
				account: model.Account{
					Email: "test@mail.com",
				},
				mailProvider:  &MockMailProvider{},
				messageReader: &MockMessageReader{},
				messageWriter: &MockMessageWriter{},
				mailboxReader: &MockMailboxReader{
					err: fmt.Errorf("can't get mailboxes"),
				},
				mailboxWriter: NewMockMailboxWriter(),
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := &ImapSqliteSynchronizer{
				Account:             tt.fields.account,
				MailProvider:        tt.fields.mailProvider,
				RemoteMessageReader: tt.fields.messageReader,
				MessageWriter:       tt.fields.messageWriter,
				MailboxReader:       tt.fields.mailboxReader,
				MailboxWriter:       tt.fields.mailboxWriter,
			}
			if err := i.SyncMailBoxes(); (err != nil) != tt.wantErr {
				t.Errorf("SyncMailBoxes() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr {
				mr := tt.fields.mailboxReader.(*MockMailboxReader)
				mw := tt.fields.mailboxWriter.(*MockMailboxWriter)
				require.Equal(t, mr.mailboxes, mw.mailboxes)
			}
		})
	}
}

func getUids(mailbox model.Mailbox, messages map[string][]model.Message) []uint32 {
	uids := make([]uint32, len(messages[mailbox.Name]))
	for i, message := range messages[mailbox.Name] {
		uids[i] = message.Uid
	}
	return uids
}

func getMessages(mailbox model.Mailbox, uids []uint32, messages map[string][]model.Message) []model.Message {
	msgs := make([]model.Message, len(uids))
	for i, uid := range uids {
		for _, msg := range messages[mailbox.Name] {
			if msg.Uid == uid {
				msgs[i] = msg
				break
			}
		}
	}
	return msgs
}

func asMessages(messages []WholeMessage) []model.Message {
	msgs := make([]model.Message, len(messages))
	for i, message := range messages {
		msgs[i] = message.Message
	}
	return msgs
}
