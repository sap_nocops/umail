package middleware

import "gitlab.com/sap_nocops/umail/pkg/model"

type MockMailProvider struct {
	loginCalledCnt  int
	logoutCalledCnt int
}

func (m *MockMailProvider) Login() error {
	m.loginCalledCnt++
	return nil
}

func (m *MockMailProvider) Logout() error {
	m.logoutCalledCnt++
	return nil
}

type MockMessageReader struct {
	messages map[string][]model.Message
}

func (m *MockMessageReader) GetMessagesUids(mailbox model.Mailbox) ([]uint32, error) {
	uids := make([]uint32, len(m.messages[mailbox.Name]))
	for i, message := range m.messages[mailbox.Name] {
		uids[i] = message.Uid
	}
	return uids, nil
}

func (m *MockMessageReader) GetMessages(mailbox model.Mailbox, from, to uint32) ([]model.Message, error) {
	return m.messages[mailbox.Name][from : to+1], nil
}

func (m *MockMessageReader) GetMessage(mailbox model.Mailbox, uid uint32) (model.Message, error) {
	return m.messages[mailbox.Name][uid], nil
}

func (m *MockMessageReader) GetTotalMessages(mailbox model.Mailbox) (uint32, error) {
	return uint32(len(m.messages[mailbox.Name])), nil
}

type WholeMessage struct {
	Message model.Message
	Body    model.MessageBody
}

type MockMessageWriter struct {
	createdMessages map[string][]WholeMessage
	deletedMessages map[string][]uint32
}

func NewMockMessageWriter() *MockMessageWriter {
	return &MockMessageWriter{
		createdMessages: make(map[string][]WholeMessage),
		deletedMessages: make(map[string][]uint32),
	}
}

func (m *MockMessageWriter) CreateMessage(mailbox model.Mailbox, message model.Message, body model.MessageBody) error {
	if _, ok := m.createdMessages[mailbox.Name]; !ok {
		m.createdMessages[mailbox.Name] = make([]WholeMessage, 0)
	}
	m.createdMessages[mailbox.Name] = append(m.createdMessages[mailbox.Name], WholeMessage{
		Message: message,
		Body:    body,
	})
	return nil
}

func (m *MockMessageWriter) DeleteMessage(mailbox model.Mailbox, uid uint32) error {
	if _, ok := m.deletedMessages[mailbox.Name]; !ok {
		m.deletedMessages[mailbox.Name] = make([]uint32, 0)
	}
	m.deletedMessages[mailbox.Name] = append(m.deletedMessages[mailbox.Name], uid)
	return nil
}

type MockMailboxReader struct {
	mailboxes []model.Mailbox
	err       error
}

func (m *MockMailboxReader) GetMailboxes() ([]model.Mailbox, error) {
	if m.err != nil {
		return nil, m.err
	}
	return m.mailboxes, nil
}

type MockMailboxWriter struct {
	mailboxes []model.Mailbox
}

func NewMockMailboxWriter() *MockMailboxWriter {
	return &MockMailboxWriter{mailboxes: make([]model.Mailbox, 0)}
}

func (m *MockMailboxWriter) SaveMailboxes(mailboxes ...model.Mailbox) error {
	for _, mailbox := range mailboxes {
		m.mailboxes = append(m.mailboxes, mailbox)
	}
	return nil
}
