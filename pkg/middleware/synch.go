package middleware

import (
	"gitlab.com/sap_nocops/umail/pkg/model"
	"gitlab.com/sap_nocops/umail/pkg/provider"
)

type Synchronizer interface {
	SyncMail(mailbox model.Mailbox) error
	SyncMailBoxes() error
}

type ImapSqliteSynchronizer struct {
	Account             model.Account
	MailProvider        provider.MailProvider
	RemoteMessageReader provider.MessageReaderProvider
	LocalMessageReader  provider.MessageReaderProvider
	MessageWriter       provider.MessageWriterProvider
	MailboxReader       provider.MailboxReaderProvider
	MailboxWriter       provider.MailboxWriterProvider
}

func (i *ImapSqliteSynchronizer) SyncMail(mailbox model.Mailbox) error {
	//TODO find a way to know if something changed since last sync asking directly to the server
	//how it is done now could be expensive in terms of cpu and bandwidth
	remoteUids, err := i.RemoteMessageReader.GetMessagesUids(mailbox)
	if err != nil {
		return err
	}
	localUids, err := i.LocalMessageReader.GetMessagesUids(mailbox)
	if err != nil {
		return err
	}
	toAdd, toRemove := getDiffs(remoteUids, localUids)
	for _, uid := range toRemove {
		err := i.MessageWriter.DeleteMessage(mailbox, uid)
		if err != nil {
			return err
		}
	}
	//TODO get all messages in one call
	messages := make([]model.Message, len(toAdd))
	for j, uid := range toAdd {
		message, err := i.RemoteMessageReader.GetMessage(mailbox, uid)
		if err != nil {
			return err
		}
		messages[j] = message
	}
	for _, message := range messages {
		err := i.MessageWriter.CreateMessage(mailbox, message, model.MessageBody{})
		if err != nil {
			return err
		}
	}
	return nil
}

func (i *ImapSqliteSynchronizer) SyncMailBoxes() error {
	mailboxes, err := i.MailboxReader.GetMailboxes()
	if err != nil {
		return err
	}
	err = i.MailboxWriter.SaveMailboxes(mailboxes...)
	if err != nil {
		return err
	}
	return nil
}

//returns two slices, the first one containing all the uids which are in 'uidsA' but not in 'uidsB',
//the second one containing all the uids which are in 'uidsB' but not in 'uidsA'
func getDiffs(uidsA []uint32, uidsB []uint32) ([]uint32, []uint32) {
	aMap := make(map[uint32]bool)
	justInA := make([]uint32, 0)
	justInB := make([]uint32, 0)
	for _, uidA := range uidsA {
		found := false
		aMap[uidA] = true
		for _, uidB := range uidsB {
			if uidA == uidB {
				found = true
				break
			}
		}
		if !found {
			justInA = append(justInA, uidA)
		}
	}
	for _, uidB := range uidsB {
		if _, found := aMap[uidB]; !found {
			justInB = append(justInB, uidB)
		}
	}
	return justInA, justInB
}
