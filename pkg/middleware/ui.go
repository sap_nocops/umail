package middleware

import (
	"gitlab.com/sap_nocops/umail/pkg/model"
	"gitlab.com/sap_nocops/umail/pkg/provider"
)

type UiBridge interface {
	GetProfiles()
	GetAccounts(profile model.Profile)
}

type QmlUiBridge struct {
	profileProvider provider.ProfileProvider
	accountProvider provider.AccountProvider
	Profiles        []model.Profile
	Accounts        map[string][]model.Account
}

func NewQmlUiBridge(profileProvider provider.ProfileProvider, accountProvider provider.AccountProvider) *QmlUiBridge {
	return &QmlUiBridge{profileProvider: profileProvider, accountProvider: accountProvider}
}

func (q *QmlUiBridge) GetProfiles() {
	panic("implement me")
}

func (q *QmlUiBridge) GetAccounts(profile model.Profile) {
	panic("implement me")
}
